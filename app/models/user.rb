class User < ActiveRecord::Base
 
 
  SCREEN_NAME_SIZE       = 20
  PASSWORD_SIZE          = 10
  EMAIL_SIZE             = 30
  SCREEN_NAME_MIN_LENGTH = 4
  SCREEN_NAME_MAX_LENGTH = 20
  PASSWORD_MIN_LENGTH    = 4
  PASSWORD_MAX_LENGTH    = 40
  EMAIL_MAX_LENGTH       = 50
  SCREEN_NAME_RANGE      = SCREEN_NAME_MIN_LENGTH..SCREEN_NAME_MAX_LENGTH
  PASSWORD_RANGE         = PASSWORD_MIN_LENGTH..PASSWORD_MAX_LENGTH
 
 
  validates_uniqueness_of   :username, :email
 
  validates_length_of           :username, :within  => SCREEN_NAME_RANGE
  validates_length_of           :password, :within  => PASSWORD_RANGE
  validates_length_of           :email,       :maximum => EMAIL_MAX_LENGTH
 
  
end
